
import java.io.PrintWriter;

import org.chocosolver.samples.AbstractProblem;
import org.chocosolver.solver.ResolutionPolicy;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.IntConstraintFactory;
import org.chocosolver.solver.constraints.LCF;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.solution.Solution;
import org.chocosolver.solver.search.strategy.IntStrategyFactory;
import org.chocosolver.solver.trace.Chatterbox;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.chocosolver.solver.variables.VariableFactory;

public class Step2Pos extends AbstractProblem {

	private int[] SHIFT;
	public int nk; // Number of columns with SBoxes in the key shedule
	public int KC; // Number of columns of the initial key
	public int BC; // Number of columns of state

	public int n;  // Number of rounds
	
	// Differencial bytes
	public IntVar[][][] DY;
	public IntVar[][][] DX;
	public IntVar[][][] DSR;
	public IntVar[][] DK;
	public IntVar[][][] DeltaSB; 
	public IntVar[][]   DeltaSK; // The output of the sboxes of the key
	
	//The probability variables for the state and the key
	public IntVar[][][] p;
	public IntVar[][] pk;
	
	// Search strategy for table constraints
	public String strategy= "FC";

	// Differential bits received as input from step 1
	public int[][][] DXS1;
	public int[][] DKS1;

	public PrintWriter writer;
	
	// All the probabilities
	public IntVar[] allProbas;

	// The objective variable
	public IntVar obj;
	// The number of SBoxes
	public int nbSB;

	public Step2Pos(PrintWriter file, int[][][] X, int[][] dKS12, int nb, int Kc, int Bc, int nbSb){
		
		n = nb;
		KC=Kc;
		BC=Bc;

		int tmp;
		
		if (KC<8)
			nk=(n*BC)/KC;
		else nk=n*BC/4-1;
		writer= file;
		DKS1=dKS12;
		DXS1=X;
	
		if (BC<7)
			SHIFT=new int[]{0,1,2,3};
		else if (BC==7) 
			SHIFT=new int[]{0,1,2,4};
		else
			SHIFT=new int[]{0,1,3,4};

	

		
		DY = new IntVar[n][BC][4];
		DX = new IntVar[n][BC][4];
		DK = new IntVar[n*BC][4];
		DSR = new IntVar[n-1][BC][4];
		DeltaSB= new IntVar[n][BC][4];
		p=new IntVar[n][BC][4];
		DeltaSK= new IntVar[nk][4];				    
		pk=new IntVar[nk][4];
		nbSB=nbSb;
		this.execute(); // runs createSolver, configureSearch, buildModel, and Solve 

	}
	
	
	@Override
	// Sets the black box search strategies domain over weighted degree, and lastConflict
	public void configureSearch() {
		solver.set(
			IntStrategyFactory.domOverWDeg(solver.retrieveIntVars(), 0));//,IntStrategyFactory.minDom_UB(getAllVar()));
			solver.set(IntStrategyFactory.lastConflict(solver,solver.getStrategy()));
			this.level=Level.SILENT;
		//	SearchMonitorFactory.geometrical(solver, 100000000, 10, new TimeCounter(solver, 1), 10);
		//	SearchMonitorFactory.geometrical(solver, 500, 1.3	, new FailCounter(solver, 1), 1000);
	}



	@Override
	//Creates the solver
	public void createSolver() {
		solver=new Solver("step2");
	}




	@Override
	// Starts the search for an optimal solution, prints it to the output file and displays resolution statistics
	public void solve() {
		System.out.println("Beginning solving");		
		solver.findOptimalSolution(ResolutionPolicy.MINIMIZE,obj);
		prettyOut();
		Chatterbox.printShortStatistics(solver);
	}


	@Override
	// Initializes the variables according to the input received from Step 1, and defines the constraints
	// If the corresponding value from step1 is 0, then the domain is 0. Otherwise it is [1,255]
	public void buildModel() {
		int r,j,indP, cproba=0, csb=0;;

		for (int J=0;J<BC*n;J++) {
			r=J/BC;
			j=J%BC;
			if (KC<8)
				indP=J/KC;
			else 
				indP=J/4-1;
			
			//   DSR
			if (r<n-1){					
				DSR[r] = VF.boundedMatrix("DeltaSR["+r+"][", BC,4 ,0, 255, solver);
			}
			
			// DY
			DY[r] = VF.boundedMatrix("DeltaY["+r+"][", BC,4 ,0, 255, solver);

			for (int i=0;i<4;i++) {
				
				// Delta Y, Delta X and Delta SB
					if 	(DXS1[r][i][j] == 0) {
						DeltaSB[r][j][i] = VariableFactory.fixed(0, solver);
						DX[r][j][i] = VariableFactory.fixed(0, solver);
						p[r][j][i]=VF.fixed(0, solver);
					}
					else {
						DeltaSB[r][j][i] = VariableFactory.bounded("DeltaS["+(r)+"]["+i+"]["+j+"]", 1, 255, solver);
						DX[r][j][i] = VariableFactory.bounded("DeltaX["+(r+1)+"]["+i+"]["+j+"]", 1, 255, solver);					
						p[r][j][i]=VF.enumerated("p["+r+"]["+i+"]["+j+"]",new int[]{6,7}, solver);
						csb++;
					}
				
				// Delta K
				if (J % KC == KC-1){ 
					if (DKS1[indP][i]==0) {
						DK[J][i] = VariableFactory.fixed(0, solver);
						DeltaSK[indP][(i+3)%4]=VF.fixed(0, solver);
						pk[indP][(i+3)%4]=VF.fixed(0, solver);
					}
					else {
						DK[J][i] = VariableFactory.bounded("DeltaK["+J+"]["+i+"]", 1, 255, solver);
						DeltaSK[indP][(i+3)%4]=VariableFactory.bounded("DeltaSK["+r+"]["+i+"]", 1, 255, solver);
						pk[indP][(i+3)%4]=VF.enumerated("pk["+r+"]["+i+"]",new int[]{6,7}, solver);
						csb++;
						//nbSB++;
					}
				}
				else if (KC==8 && J % KC == 3 && J>4){ 
					if (DKS1[indP][i]==0) {
						DK[J][i] = VariableFactory.fixed(0, solver);
						DeltaSK[indP][i]=VF.fixed(0, solver);
						pk[indP][i]=VF.fixed(0, solver);
					}
					else {
						DK[J][i] = VariableFactory.bounded("DeltaK["+J+"]["+i+"]", 1, 255, solver);
						DeltaSK[indP][i]=VariableFactory.bounded("DeltaSK["+r+"]["+i+"]", 1, 255, solver);
						pk[indP][i]=VF.enumerated("pk["+r+"]["+i+"]",new int[]{6,7}, solver);
						csb++;
						//nbSB++;
					}
				}
				else {
					DK[J][i] = VariableFactory.bounded("DeltaK["+J+"]["+i+"]", 0, 255, solver);
				}		
			}
		}
		

//			IntVar toSumX[] = new IntVar[4*BC];			
//			for (j=0;j<4;j++) {
//				for (int k=0;k<BC;k++) {
//					toSumX[j*BC+k]=ICF.arithm(DX[n-1][k][j],"!=",0).reif();
//				}
//			}
//			solver.post(ICF.sum(toSumX,"=",VF.fixed(nbSB-csb,solver)));
//			
		//SB for the other rounds
		for(int i=0;i<n;i++) {
			postSB(DX[i],DeltaSB[i],p[i], solver);
		}
		// Mixcolumns in both directions
		for (int i=0;i<n-1;i++) { 
			postMC(DSR[i],DY[i+1],solver);
			postMCInv(DY[i+1],DSR[i],solver);
		}
		// Shiftrows
		for (int i=0; i<n-1; i++) {
			postSR(DeltaSB[i],DSR[i], solver);
		}
		
		// Add round key
		postARK(DY,DK, DX, solver);
		//	printStep1();
		// The key schedule
		postKS();
		
		// Get all the probability variables in a flat table
		allProbas=new IntVar[nk*4+n*BC*4];
		for (r=0;r<n;r++) {
			for (int i=0;i<4;i++) {
				for (j=0;j<BC;j++) {
					allProbas[cproba++]=p[r][j][i];
				}
			}
		}
		for (int i=0; i<nk; i++) 
			for (j=0;j<4;j++)
				allProbas[cproba++]=pk[i][j];
		
 		// Define the objective variable
		obj=VF.bounded("objs2", 6*nbSB, 7*nbSB, solver);
		// Define the sum to optimize
		solver.post(ICF.sum(allProbas, "=",obj));   
	}
	


	// x,y,z belong to the XOR tuples
	public void postXorByte(IntVar x, IntVar y, IntVar z, Solver solver){// x xor y = z
		solver.post(IntConstraintFactory.table(new IntVar[]{x,y,z}, tupleXor, strategy));
	}

	
	// Add round key
	public void postARK(IntVar[][][] dY2, IntVar[][] y, IntVar[][][] dX2, Solver solver){
		for (int r=0;r<n;r++) {
			for (int i=0; i<4; i++){
				for (int j=0; j<BC; j++){
					postXorByte(dY2[r][j][i],y[r*BC+j][i],dX2[r][j][i],solver);
				}
			}
		}
	}

	// At round 0, all active SBs can be crossed with maximal proba (since the choice of the plaintext difference is free)
	public void postSBR0(IntVar[][] in, IntVar[][] out,IntVar[][] p, Solver solver) {
		for (int i=0; i<4; i++){
			for (int j=0;j<4;j++) {
				LCF.ifThenElse(
						ICF.arithm(in[j][i],"=",0),
						LCF.and(
								ICF.arithm(out[j][i],"=",0),
								ICF.arithm(p[j][i],"=",0)
								),
								LCF.and(
										ICF.arithm(p[j][i],"=",6),
										ICF.table(new IntVar[]{in[j][i], out[j][i],p[j][i]}, tupleSB, strategy))
						);
			}

		}	
	}

	// Enforces the mixcolumns constraint
	public void postMC(IntVar[][] SR, IntVar[][] Y, Solver solver) {
		for (int j=0; j<BC; j++){
			IntVar v1 = VariableFactory.bounded("v1", 0, 255, solver);
			IntVar v2 = VariableFactory.bounded("v2", 0, 255, solver);
			IntVar v3 = VariableFactory.bounded("v3", 0, 255, solver);
			IntVar v4 = VariableFactory.bounded("v4", 0, 255, solver);
			IntVar v5 = VariableFactory.bounded("v5", 0, 255, solver);
			IntVar v6 = VariableFactory.bounded("v6", 0, 255, solver);
			IntVar v7 = VariableFactory.bounded("v7", 0, 255, solver);
			IntVar v8 = VariableFactory.bounded("v8", 0, 255, solver);	     

			solver.post(IntConstraintFactory.table(new IntVar[]{v1, SR[j][0], SR[j][1]}, Step2Pos.tupleMul2xorMul3, strategy)); // v1 = MUL2*SR[0][i] xor MUL3*SR[1][i]
			postXorByte(v1,SR[j][2],v2,solver); // v2 = v1 xor SR[2][i]
			postXorByte(v2,SR[j][3],Y[j][0],solver); // Y[0][i] = v2 xor SR[3][i]

			solver.post(IntConstraintFactory.table(new IntVar[]{v3, SR[j][1], SR[j][2]}, Step2Pos.tupleMul2xorMul3, strategy)); // v3 = MUL2*SR[1][i] xor MUL3*SR[2][i]
			postXorByte(v3,SR[j][0],v4,solver); // v4 = v3 xor SR[0][i]
			postXorByte(v4,SR[j][3],Y[j][1],solver); // Y[1][i] = v4 xor SR[3][i]
			solver.post(IntConstraintFactory.table(new IntVar[]{v5, SR[j][2], SR[j][3]}, Step2Pos.tupleMul2xorMul3, strategy)); // v5 = MUL2*SR[2][i] xor MUL3*SR[3][i]
			postXorByte(v5,SR[j][0],v6,solver); // v6 = v5 xor SR[2][i]
			postXorByte(v6,SR[j][1],Y[j][2],solver); // Y[2][i] = v6 xor SR[1][i]
			solver.post(IntConstraintFactory.table(new IntVar[]{v7, SR[j][3], SR[j][0]}, Step2Pos.tupleMul2xorMul3, strategy)); // v7 = MUL2*SR[3][i] xor MUL3*SR[0][i]
			postXorByte(v7,SR[j][1],v8,solver); // v8 = v7 xor SR[1][i]
			postXorByte(v8,SR[j][2],Y[j][3],solver); // Y[3][i] = v8 xor SR[2][i]
		}

	}

	// Shift rows
	public void postSR(IntVar[][] xIn, IntVar[][] xOut, Solver solver) {
		for (int j=0; j<BC; j++){
			for (int i=0;i<4;i++) {
				solver.post(IntConstraintFactory.arithm(xIn[(j+SHIFT[i])%BC][i], "=", xOut[j][i]));
			}
		}
	}

	// SB relation
	public void postSB(IntVar[][] xIn, IntVar[][] xOut, IntVar[][] p, Solver solver) {
		for (int i=0; i<4; i++){
			for (int j=0;j<BC;j++) {
				LCF.ifThenElse(
						ICF.arithm(xIn[j][i],"=",0),
						LCF.and(
								ICF.arithm(xOut[j][i],"=",0),
								ICF.arithm(p[j][i],"=",0)
								),
								ICF.table(new IntVar[]{xIn[j][i], xOut[j][i],p[j][i]}, tupleSB, strategy)
						);
			}

		}
	}

	// The inverse mixcolumn operation
	public void postMCInv(IntVar[][] DX, IntVar[][] SR,  Solver solver) {

		for (int j=0; j<BC; j++){
			IntVar[] tmp=new IntVar[]{DX[j][0],DX[j][1],DX[j][2],DX[j][3]};
			IntVar[] tmp2=new IntVar[]{SR[j][0],SR[j][1],SR[j][2],SR[j][3]};

			LCF.ifThen(
					ICF.sum(tmp,"=",VF.fixed(0,solver)),
					ICF.sum(tmp2,"=",VF.fixed(0,solver))
			);


			IntVar[] tmpMC=VF.boundedArray("tmpMC", 24, 0, 255, solver);

			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[0], tmpMC[0]}, Step2Pos.mul14, strategy));
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[1], tmpMC[1]}, Step2Pos.mul11, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[2], tmpMC[2]}, Step2Pos.mul13, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[3], tmpMC[3]}, Step2Pos.mul9, strategy)); 

			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[0], tmpMC[4]}, Step2Pos.mul9, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[1], tmpMC[5]}, Step2Pos.mul14, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[2], tmpMC[6]}, Step2Pos.mul11, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[3], tmpMC[7]}, Step2Pos.mul13, strategy)); 

			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[0], tmpMC[8]}, Step2Pos.mul13, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[1], tmpMC[9]}, Step2Pos.mul9, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[2], tmpMC[10]}, Step2Pos.mul14, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[3], tmpMC[11]}, Step2Pos.mul11, strategy)); 

			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[0], tmpMC[12]}, Step2Pos.mul11, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[1], tmpMC[13]}, Step2Pos.mul13, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[2], tmpMC[14]}, Step2Pos.mul9, strategy)); 
			solver.post(IntConstraintFactory.table(new IntVar[]{tmp[3], tmpMC[15]}, Step2Pos.mul14, strategy)); 

			postXorByte(tmpMC[0],tmpMC[1],tmpMC[16],solver); 
			postXorByte(tmpMC[2],tmpMC[3],tmpMC[17],solver); 
			postXorByte(tmpMC[16],tmpMC[17],tmp2[0],solver); 

			postXorByte(tmpMC[4],tmpMC[5],tmpMC[18],solver); 
			postXorByte(tmpMC[6],tmpMC[7],tmpMC[19],solver); 
			postXorByte(tmpMC[18],tmpMC[19],tmp2[1],solver); 

			postXorByte(tmpMC[8],tmpMC[9],tmpMC[20],solver); 
			postXorByte(tmpMC[10],tmpMC[11],tmpMC[21],solver); 
			postXorByte(tmpMC[20],tmpMC[21],tmp2[2],solver); 

			postXorByte(tmpMC[12],tmpMC[13],tmpMC[22],solver); 
			postXorByte(tmpMC[14],tmpMC[15],tmpMC[23],solver); 
			postXorByte(tmpMC[22],tmpMC[23],tmp2[3],solver); 

		}
	}
	
	// Lookup table for multiplying by 9 in Rijndael's finite field
	public static Tuples mul9=initMul9();
	public static Tuples initMul9() {
		Tuples ret=new Tuples(true);
		int tab[]= {0x00,0x09,0x12,0x1b,0x24,0x2d,0x36,0x3f,0x48,0x41,0x5a,0x53,0x6c,0x65,0x7e,0x77,
				0x90,0x99,0x82,0x8b,0xb4,0xbd,0xa6,0xaf,0xd8,0xd1,0xca,0xc3,0xfc,0xf5,0xee,0xe7,
				0x3b,0x32,0x29,0x20,0x1f,0x16,0x0d,0x04,0x73,0x7a,0x61,0x68,0x57,0x5e,0x45,0x4c,
				0xab,0xa2,0xb9,0xb0,0x8f,0x86,0x9d,0x94,0xe3,0xea,0xf1,0xf8,0xc7,0xce,0xd5,0xdc,
				0x76,0x7f,0x64,0x6d,0x52,0x5b,0x40,0x49,0x3e,0x37,0x2c,0x25,0x1a,0x13,0x08,0x01,
				0xe6,0xef,0xf4,0xfd,0xc2,0xcb,0xd0,0xd9,0xae,0xa7,0x4,0xb5,0x8a,0x83,0x98,0x91,
				0x4d,0x44,0x5f,0x56,0x69,0x60,0x7b,0x72,0x05,0x0c,0x17,0x1e,0x21,0x28,0x33,0x3a,
				0xdd,0xd4,0xcf,0xc6,0xf9,0xf0,0xeb,0xe2,0x95,0x9c,0x87,0x8e,0xb1,0xb8,0xa3,0xaa,
				0xec,0xe5,0xfe,0xf7,0xc8,0xc1,0xda,0xd3,0xa4,0xad,0xb6,0xbf,0x80,0x89,0x92,0x9b,
				0x7c,0x75,0x6e,0x67,0x58,0x51,0x4a,0x43,0x34,0x3d,0x26,0x2f,0x10,0x19,0x02,0x0b,
				0xd7,0xde,0xc5,0xcc,0xf3,0xfa,0xe1,0xe8,0x9f,0x96,0x8d,0x84,0xbb,0xb2,0xa9,0xa0,
				0x47,0x4e,0x55,0x5c,0x63,0x6a,0x71,0x78,0x0f,0x06,0x1d,0x14,0x2b,0x22,0x39,0x30,
				0x9a,0x93,0x88,0x81,0xbe,0xb7,0xac,0xa5,0xd2,0xdb,0xc0,0xc9,0xf6,0xff,0xe4,0xed,
				0x0a,0x03,0x18,0x11,0x2e,0x27,0x3c,0x35,0x42,0x4b,0x50,0x59,0x66,0x6f,0x74,0x7d,
				0xa1,0xa8,0xb3,0xba,0x85,0x8c,0x97,0x9e,0xe9,0xe0,0xfb,0xf2,0xcd,0xc4,0xdf,0xd6,
				0x31,0x38,0x23,0x2a,0x15,0x1c,0x07,0x0e,0x79,0x70,0x6b,0x62,0x5d,0x54,0x4f,0x46};

		for (int i=0;i<256;i++) {
			ret.add(i,tab[i]);
		}

		return ret;
	}
	// Lookup table for multiplying by 11 in Rijndael's finite field
	public static Tuples mul11=initMul11();
	public static Tuples initMul11() {
		Tuples ret=new Tuples(true);
		int tab[]= {0x00,0x0b,0x16,0x1d,0x2c,0x27,0x3a,0x31,0x58,0x53,0x4e,0x45,0x74,0x7f,0x62,0x69,
				0xb0,0xbb,0xa6,0xad,0x9c,0x97,0x8a,0x81,0xe8,0xe3,0xfe,0xf5,0xc4,0xcf,0xd2,0xd9,
				0x7b,0x70,0x6d,0x66,0x57,0x5c,0x41,0x4a,0x23,0x28,0x35,0x3e,0x0f,0x04,0x19,0x12,
				0xcb,0xc0,0xdd,0xd6,0xe7,0xec,0xf1,0xfa,0x93,0x98,0x85,0x8e,0xbf,0xb4,0xa9,0xa2,
				0xf6,0xfd,0xe0,0xeb,0xda,0xd1,0xcc,0xc7,0xae,0xa5,0xb8,0xb3,0x82,0x89,0x94,0x9f,
				0x46,0x4d,0x50,0x5b,0x6a,0x61,0x7c,0x77,0x1e,0x15,0x08,0x03,0x32,0x39,0x24,0x2f,
				0x8d,0x86,0x9b,0x90,0xa1,0xaa,0xb7,0x4,0xd5,0xde,0xc3,0xc8,0xf9,0xf2,0xef,0xe4,
				0x3d,0x36,0x2b,0x20,0x11,0x1a,0x07,0x0c,0x65,0x6e,0x73,0x78,0x49,0x42,0x5f,0x54,
				0xf7,0xfc,0xe1,0xea,0xdb,0xd0,0xcd,0xc6,0xaf,0xa4,0xb9,0xb2,0x83,0x88,0x95,0x9e,
				0x47,0x4c,0x51,0x5a,0x6b,0x60,0x7d,0x76,0x1f,0x14,0x09,0x02,0x33,0x38,0x25,0x2e,
				0x8c,0x87,0x9a,0x91,0xa0,0xab,0xb6,0xbd,0xd4,0xdf,0xc2,0xc9,0xf8,0xf3,0xee,0xe5,
				0x3c,0x37,0x2a,0x21,0x10,0x1b,0x06,0x0d,0x64,0x6f,0x72,0x79,0x48,0x43,0x5e,0x55,
				0x01,0x0a,0x17,0x1c,0x2d,0x26,0x3b,0x30,0x59,0x52,0x4f,0x44,0x75,0x7e,0x63,0x68,
				0xb1,0xba,0xa7,0xac,0x9d,0x96,0x8b,0x80,0xe9,0xe2,0xff,0xf4,0xc5,0xce,0xd3,0xd8,
				0x7a,0x71,0x6c,0x67,0x56,0x5d,0x40,0x4b,0x22,0x29,0x34,0x3f,0x0e,0x05,0x18,0x13,
				0xca,0xc1,0xdc,0xd7,0xe6,0xed,0xf0,0xfb,0x92,0x99,0x84,0x8f,0xbe,0xb5,0xa8,0xa3};

		for (int i=0;i<256;i++) {
			ret.add(i,tab[i]);
		}

		return ret;
	}	public static Tuples mul13=initMul13();
	// Lookup table for multiplying by 13 in Rijndael's finite field
	public static Tuples initMul13() {
		Tuples ret=new Tuples(true);
		int tab[]= {0x00,0x0d,0x1a,0x17,0x34,0x39,0x2e,0x23,0x68,0x65,0x72,0x7f,0x5c,0x51,0x46,0x4b,
				0xd0,0xdd,0xca,0xc7,0xe4,0xe9,0xfe,0xf3,0xb8,0xb5,0xa2,0xaf,0x8c,0x81,0x96,0x9b,
				0xbb,0xb6,0xa1,0xac,0x8f,0x82,0x95,0x98,0xd3,0xde,0xc9,0xc4,0xe7,0xea,0xfd,0xf0,
				0x6b,0x66,0x71,0x7c,0x5f,0x52,0x45,0x48,0x03,0x0e,0x19,0x14,0x37,0x3a,0x2d,0x20,
				0x6d,0x60,0x77,0x7a,0x59,0x54,0x43,0x4e,0x05,0x08,0x1f,0x12,0x31,0x3c,0x2b,0x26,
				0xbd,0xb0,0xa7,0xaa,0x89,0x84,0x93,0x9e,0xd5,0xd8,0xcf,0xc2,0xe1,0xec,0xfb,0xf6,
				0xd6,0xdb,0xcc,0xc1,0xe2,0xef,0xf8,0xf5,0xbe,0xb3,0xa4,0xa9,0x8a,0x87,0x90,0x9d,
				0x06,0x0b,0x1c,0x11,0x32,0x3f,0x28,0x25,0x6e,0x63,0x74,0x79,0x5a,0x57,0x40,0x4d,
				0xda,0xd7,0xc0,0xcd,0xee,0xe3,0xf4,0xf9,0xb2,0xbf,0xa8,0xa5,0x86,0x8b,0x9c,0x91,
				0x0a,0x07,0x10,0x1d,0x3e,0x33,0x24,0x29,0x62,0x6f,0x78,0x75,0x56,0x5b,0x4c,0x41,
				0x61,0x6c,0x7b,0x76,0x55,0x58,0x4f,0x42,0x09,0x04,0x13,0x1e,0x3d,0x30,0x27,0x2a,
				0xb1,0x4,0xab,0xa6,0x85,0x88,0x9f,0x92,0xd9,0xd4,0xc3,0xce,0xed,0xe0,0xf7,0xfa,
				0xb7,0xba,0xad,0xa0,0x83,0x8e,0x99,0x94,0xdf,0xd2,0xc5,0xc8,0xeb,0xe6,0xf1,0xfc,
				0x67,0x6a,0x7d,0x70,0x53,0x5e,0x49,0x44,0x0f,0x02,0x15,0x18,0x3b,0x36,0x21,0x2c,
				0x0c,0x01,0x16,0x1b,0x38,0x35,0x22,0x2f,0x64,0x69,0x7e,0x73,0x50,0x5d,0x4a,0x47,
				0xdc,0xd1,0xc6,0xcb,0xe8,0xe5,0xf2,0xff,0xb4,0xb9,0xae,0xa3,0x80,0x8d,0x9a,0x97};

		for (int i=0;i<256;i++) {
			ret.add(i,tab[i]);
		}

		return ret;
	}	public static Tuples mul14=initMul14();
	// Lookup table for multiplying by 14 in Rijndael's finite field

	public static Tuples initMul14() {
		Tuples ret=new Tuples(true);
		int tab[]= {0x00,0x0e,0x1c,0x12,0x38,0x36,0x24,0x2a,0x70,0x7e,0x6c,0x62,0x48,0x46,0x54,0x5a,
				0xe0,0xee,0xfc,0xf2,0xd8,0xd6,0xc4,0xca,0x90,0x9e,0x8c,0x82,0xa8,0xa6,0xb4,0xba,
				0xdb,0xd5,0xc7,0xc9,0xe3,0xed,0xff,0xf1,0xab,0xa5,0xb7,0xb9,0x93,0x9d,0x8f,0x81,
				0x3b,0x35,0x27,0x29,0x03,0x0d,0x1f,0x11,0x4b,0x45,0x57,0x59,0x73,0x7d,0x6f,0x61,
				0xad,0xa3,0xb1,0xbf,0x95,0x9b,0x89,0x87,0xdd,0xd3,0xc1,0xcf,0xe5,0xeb,0xf9,0xf7,
				0x4d,0x43,0x51,0x5f,0x75,0x7b,0x69,0x67,0x3d,0x33,0x21,0x2f,0x05,0x0b,0x19,0x17,
				0x76,0x78,0x6a,0x64,0x4e,0x40,0x52,0x5c,0x06,0x08,0x1a,0x14,0x3e,0x30,0x22,0x2c,
				0x96,0x98,0x8a,0x84,0xae,0xa0,0xb2,0x4,0xe6,0xe8,0xfa,0xf4,0xde,0xd0,0xc2,0xcc,
				0x41,0x4f,0x5d,0x53,0x79,0x77,0x65,0x6b,0x31,0x3f,0x2d,0x23,0x09,0x07,0x15,0x1b,
				0xa1,0xaf,0xbd,0xb3,0x99,0x97,0x85,0x8b,0xd1,0xdf,0xcd,0xc3,0xe9,0xe7,0xf5,0xfb,
				0x9a,0x94,0x86,0x88,0xa2,0xac,0xbe,0xb0,0xea,0xe4,0xf6,0xf8,0xd2,0xdc,0xce,0xc0,
				0x7a,0x74,0x66,0x68,0x42,0x4c,0x5e,0x50,0x0a,0x04,0x16,0x18,0x32,0x3c,0x2e,0x20,
				0xec,0xe2,0xf0,0xfe,0xd4,0xda,0xc8,0xc6,0x9c,0x92,0x80,0x8e,0xa4,0xaa,0xb8,0xb6,
				0x0c,0x02,0x10,0x1e,0x34,0x3a,0x28,0x26,0x7c,0x72,0x60,0x6e,0x44,0x4a,0x58,0x56,
				0x37,0x39,0x2b,0x25,0x0f,0x01,0x13,0x1d,0x47,0x49,0x5b,0x55,0x7f,0x71,0x63,0x6d,
				0xd7,0xd9,0xcb,0xc5,0xef,0xe1,0xf3,0xfd,0xa7,0xa9,0xbb,0xb5,0x9f,0x91,0x83,0x8d};

		for (int i=0;i<256;i++) {
			ret.add(i,tab[i]);
		}

		return ret;
	}





	public void postKS() {
		int indP;

		for (int J=KC-1;J<n*BC;J++) {
			if (KC<8)
				indP=(J/KC);
			else
				indP=(J/4-1);
			for (int i=0;i<4;i++) {		
				if (J<BC*n-1) {
					if (J%KC==(KC-1)) {
						solver.post(ICF.table(new IntVar[]{DK[J][(i+1)%4], DeltaSK[indP][i],pk[indP][i]},tupleSB, strategy));
					}
					else if (KC==8 && J%KC==3) {
						solver.post(ICF.table(new IntVar[]{DK[J][i], DeltaSK[indP][i],pk[indP][i]},tupleSB, strategy));
					}
				}
				if (J%KC==0) {
					postXorByte(DK[J-KC][i],DeltaSK[indP-1][i],DK[J][i],solver);
				}
				else if (KC==8 && J % KC==4) {
					postXorByte(DK[J-KC][i],DeltaSK[indP-1][i],DK[J][i],solver);
				}
				else if (J%KC>0 && J>=KC) {
					postXorByte(DK[J-1][i],DK[J-KC][i],DK[J][i],solver);
				}
			}	

		}

	}					
				
	
		



	// The SBox of the AES
	public static int[] Sbox = new int[] {
		99, 124, 119, 123, 242, 107, 111, 197,  48,   1, 103,  43, 254, 215, 171, 118, 
		202, 130, 201, 125, 250,  89,  71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 
		183, 253, 147,  38,  54,  63, 247, 204,  52, 165, 229, 241, 113, 216,  49,  21, 
		4, 199,  35, 195,  24, 150,   5, 154,   7,  18, 128, 226, 235,  39, 178, 117, 
		9, 131,  44,  26,  27, 110,  90, 160,  82,  59, 214, 179,  41, 227,  47, 132, 
		83, 209,   0, 237,  32, 252, 177,  91, 106, 203, 190,  57,  74,  76,  88, 207, 
		208, 239, 170, 251,  67,  77,  51, 133,  69, 249,   2, 127,  80,  60, 159, 168, 
		81, 163,  64, 143, 146, 157,  56, 245, 188, 182, 218,  33,  16, 255, 243, 210, 
		205,  12,  19, 236,  95, 151,  68,  23, 196, 167, 126,  61, 100,  93,  25, 115, 
		96, 129,  79, 220,  34,  42, 144, 136,  70, 238, 184,  20, 222,  94,  11, 219, 
		224,  50,  58,  10,  73,   6,  36,  92, 194, 211, 172,  98, 145, 149, 228, 121, 
		231, 200,  55, 109, 141, 213,  78, 169, 108,  86, 244, 234, 101, 122, 174,   8, 
		186, 120,  37,  46,  28, 166, 180, 198, 232, 221, 116,  31,  75, 189, 139, 138, 
		112,  62, 181, 102,  72,   3, 246,  14,  97,  53,  87, 185, 134, 193,  29, 158, 
		225, 248, 152,  17, 105, 217, 142, 148, 155,  30, 135, 233, 206,  85,  40, 223, 
		140, 161, 137,  13, 191, 230,  66, 104,  65, 153,  45,  15, 176,  84, 187,  22
	};
	public static Tuples tupleSB = Step2Pos.createRelationSbox();
	// Generates the SB tuples: {(X,Y,P) | Exists (B1,B2) in [0,255] x [0,255], X= B1 xor B2, Y= S(B1) xor S(B2), P=log_2(Pr(X->Y))}
	public static Tuples createRelationSbox(){

		int trans[][] = new int[256][127];
		int probas[][] = new int[256][256];
		int ctrans[] = new int[256];
		Tuples tuples = new Tuples(true);

		for (int i=0;i<256;i++) {
			ctrans[i]=0;
			for (int j=0;j<256;j++) {
				probas[i][j]=0;
				if(j<127)
					trans[i][j]=0;
			}
		}
		for(int i=0;i<256;i++)
		{
			for(int j=0;j<256;j++)
			{
				probas[i][(Sbox[j]^Sbox[j^i])] ++;
				if(probas[i][(Sbox[j]^Sbox[j^i])]==1) {
					trans[i][ctrans[i]++]=Sbox[j]^Sbox[i^j];
				}
			}
		}
		tuples.add(0,0,0);
		int p=0;
		for (int i=1; i<256; i++){
			for (int j=0; j<127; j++) {
				p=probas[i][trans[i][j]]/2;		
				tuples.add(i,trans[i][j],8-p);
			}
		}
		return tuples;
	}


	// Defines the XOR tuples: {(X, Y, Z) | exists (B1,B2) in [0,255] x [0,255], X=B1, Y=B2, Z= B1 xor B2}
	public static Tuples tupleXor = Step2Pos.createRelationXor();
	public static Tuples createRelationXor(){
		Tuples tuples = new Tuples(true);
				for (int i=0; i<256; i++)
					for (int j=0; j<256; j++)
						tuples.add(i,j,i^j);
				return tuples;
	}
	
	public static Tuples tupleMul2xorMul3 = Step2Pos.createRelationMul2xorMul3();
	// returns { (x,y,z) | x = Mul2*y xor Mul3*z }
	public static Tuples createRelationMul2xorMul3(){
		Tuples tuples = new Tuples(true);
		for (int i=0; i<256; i++){
			int ii;
			if (i<128) ii = 2*i;
			else ii = (2*i % 256) ^ 27;
			for (int j=0; j<256; j++){
				int jj;
				if (j<128) jj = (2*j) ^ j;
				else jj = ((2*j % 256) ^ 27) ^ j;
				tuples.add(ii ^ jj,i,j);
				
			}
		}
		return tuples;
	}
	



	@Override
	//Prints the solution to the output file
	public void prettyOut() {
	    boolean hasSol=false;
		for(Solution s:solver.getSolutionRecorder().getSolutions()){
		    hasSol=true;
			int pr=s.getIntVal(obj);
			System.out.println("Proba: "+pr);

			writer.println("Proba :2^-"+pr);
			writer.println("Round 0:");
			writer.println("       deltaX            deltaK[0]             deltaX[0]             DeltaSB[0] ");
			for (int j=0; j<4; j++){
				for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DY[0][k][j]))+" "); writer.print("     ");
				for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DK[k][j]))+" "); 

				writer.print("     ");
				for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DX[0][k][j]))+" "); writer.print("     ");
				for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DeltaSB[0][k][j]))+" "); writer.print("     ");
			//	for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(p[0][k][j]))+" "); writer.print("     ");

				writer.println();

			}
			for (int i=1; i<n; i++){
				writer.println("Round "+i+":");
				if (i<n-1)
					writer.println("       deltaSR["+(i-1)+"]             deltaY["+(i-1)+"]             deltaK["+i+"]                deltaX["+i+"]             deltaSB["+i+"] ");
				else 
					writer.println("       deltaSR["+(i-1)+"]             deltaY["+(i-1)+"]             deltaK["+i+"]             deltaX["+i+"]     ");

				
				for (int j=0; j<4; j++){
					for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DSR[i-1][k][j]))+" "); writer.print("      ");
					for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DY[i][k][j]))+" "); writer.print("      ");
					for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DK[i*BC+k][j]))+" "); writer.print("      ");

					for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DX[i][k][j]))+" "); 
					writer.print("      ");
					if (i<n-1)
						for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(DeltaSB[i][k][j]))+" "); writer.print("      ");
			//			for (int k=0; k<BC; k++) writer.printf("%3s",Integer.toHexString(s.getIntVal(p[i][k][j]))+" "); writer.print("      ");

						writer.println();
				}
			}

			writer.flush();	
		}
	}
}
