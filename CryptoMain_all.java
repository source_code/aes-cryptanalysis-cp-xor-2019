
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class CryptoMain_all {

	private static int NBROUNDS;	 // The number of rounds
	private static int OBJ;	 // The number of rounds
	private static String inputFile; // The file containing the output of the solver for step 1
	private static String outputFile;// Where to output the solution
	private static int KEY_BITS;     // The size of the key	  
	private static int KC;           // Number of columns of the key
	private static int BLOCK_BITS;     // The size of the key	  
	private static int BC;           // Number of columns of the key
	private static int S1B;           // Solving method. 1 for normal, 2 for pos

	static PrintWriter writer;


	public static void main(String[] args) throws FileNotFoundException {
//		inputFile="/home/david/david-gerault/Code_Vraiment_Propre/Picat-S1Diff-noshift/128-3-5";
		inputFile=args[0];

		KEY_BITS=Integer.parseInt(args[1]);
		BLOCK_BITS=128;//Integer.parseInt(args[2]);
		NBROUNDS=Integer.parseInt(args[2]);
		OBJ=Integer.parseInt(args[3]);
		S1B=Integer.parseInt(args[4]);
		outputFile=inputFile+"_S2";
		KC=KEY_BITS/32;
		BC=BLOCK_BITS/32;

		CryptoMain_all CM=new CryptoMain_all();
		CM.Step2();

	}

	// Parses the input file and starts a step2 every time a solution is read.
	public  void Step2() {
		int cpt = 0,cpt2=0;
		int n=NBROUNDS;
		BufferedReader in = null;
		PrintWriter writer = null;
		String line = null;
		Scanner scanner = null;
		int nbSb=OBJ;
		int NK;
		if (KC<8)
			NK=(n*BC)/KC;
		else NK=n*BC/4-1;		// The values read from step1

		if (S1B==1) {
			int[][][] DXS1;    
			int[][][] DYS1;         
			int[][][] DKS1;
			DXS1=new int[n][4][BC];DKS1=new int[n][4][BC];DYS1=new int[n-1][4][BC];
			try
			{
				in = new BufferedReader(new FileReader(inputFile));
				writer = new PrintWriter(outputFile, "UTF-8");
				scanner=new Scanner(in);
			}
			catch(FileNotFoundException | UnsupportedEncodingException exc)
			{
				exc.printStackTrace();
			}
			try {
				while ((line=in.readLine() )!= null) {
					if (line.length()>0 && (line.startsWith("delta") || line.startsWith("D"))){
						cpt2=0;		
						String[] tab=line.substring(line.indexOf("[")+1,line.indexOf("]")).split(",");
						//	System.out.println(line);

						if (line.startsWith("deltaY") || line.startsWith("DZ")) {
							for (int r=0;r<n-1;r++) {
								for (int i=0;i<4;i++) {
									for (int j=0;j<BC;j++) {
										DYS1[r][i][j]=Integer.parseInt(tab[cpt2++].trim());
									}
								}
							}
							cpt++;
						}
						else if (line.startsWith("deltaX")  || line.startsWith("DX")) {
							for (int r=0;r<n;r++) {

								for (int i=0;i<4;i++) {
									for (int j=0;j<BC;j++) {
										DXS1[r][i][j]=Integer.parseInt(tab[cpt2++].trim());
									}
								}
							}
							cpt++;

						}
						else if (line.startsWith("deltaK") || line.startsWith("DK")) {
							for (int r=0;r<n;r++) {

								for (int i=0;i<4;i++) {
									for (int j=0;j<BC+1;j++) {
										if (j<4)
											DKS1[r][i][j]=Integer.parseInt(tab[cpt2++].trim());
										else cpt2++;
									}
								}
							}
							//		for (int J=0;J<n*BC;J++) {
							//			for (int i=0;i<4;i++) {
							//				DKS1[J][i]=Integer.parseInt(tab[cpt2++].trim());
							//			}
							//	}
							cpt++;

						}
						if (cpt==3) {
								new Step2Normal(writer, DXS1, DYS1, DKS1, n, KC, BC);
								cpt=0;
							}
						}
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
			else if (S1B==2) {
				int[][][] DXS1;    
				int[][][] DYS1;         
				int[][] DKS1;
				DXS1=new int[n][4][BC];DKS1=new int[NK][4];
				try
				{
					in = new BufferedReader(new FileReader(inputFile));
					writer = new PrintWriter(outputFile, "UTF-8");
					scanner=new Scanner(in);
				}
				catch(FileNotFoundException | UnsupportedEncodingException exc)
				{
					exc.printStackTrace();
				}
				try {
					while ((line=in.readLine() )!= null) {
						if (line.length()>0 && line.startsWith("delta")  ||  (line.startsWith("D")) ) {
							cpt2=0;		
							String[] tab=line.substring(line.indexOf("[")+1,line.indexOf("]")).split(",");
							if (line.contains("X")) {
								for (int r=0;r<n;r++) {
									for (int i=0;i<4;i++) {
										for (int j=0;j<BC;j++) {
											DXS1[r][i][j]=Integer.parseInt(tab[cpt2++].trim());
										}
									}
								}
								cpt++;
							}

							else if (line.contains("SBK")) {
								for (int r=0;r<NK;r++) {
									for (int i=0;i<4;i++) {								
										DKS1[r][i]=Integer.parseInt(tab[cpt2++].trim());
									}
								}
								cpt++;
							}

							if (cpt==2) {
								new Step2Pos(writer, DXS1, DKS1, n, KC, BC, nbSb);
								cpt=0;
							}
						}
					}
				} catch (NumberFormatException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}
