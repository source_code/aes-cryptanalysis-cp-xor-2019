JAVAC=javac
sources = $(wildcard *.java)
classes = $(sources:.java=.class)
all: $(classes)

%.class : %.java
	$(JAVAC) -sourcepath ./ -cp $(CHOCO_DIR)/choco-solver-3.3.3-with-dependencies.jar $<

clean :
	rm -f *.class

