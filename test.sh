#/bin/bash
CHOCO_DIR="/home/david"
PICAT_DIR="/home/david/Picat"
MINIZINC_DIR="/home/david/MiniZincIDE-2.2.3-bundle-linux-x86_64/bin"


KB=128; # Number of bits in the key
n=3;    # Number of rounds
obj=5;  # Number of active SBoxes 
make CHOCO_DIR="$CHOCO_DIR"  &> /dev/null;

function solve_shift() {
 file=$1
 outSols=$2
 DX="Not empty"
 cat Heuristics/shift_picat >> /tmp/$file.mzn
 i=0
 while [[ $DX != '' ]]; do
  $MINIZINC_DIR/mzn2fzn /tmp/$file.mzn -D "KEY_BITS=$KB;r=$n; objStep1=$obj";
  sh -c "time $PICAT_DIR/picat $PICAT_DIR/lib/fzn_picat_sat.pi /tmp/$file.fzn " 2&> /tmp/$file-time;
  DX=$(cat /tmp/$file-time | grep 'DX' | sed "s/DX/DX$i/g" );
  DSBK=$(cat /tmp/$file-time | grep 'DSBK' | sed "s/DSBK/DSBK$i/g" );
  time=$(cat /tmp/$file-time | grep 'user\|sys' );
  if [[ $DX != '' ]]; then
   echo "array [0..r-1, 0..3, 0..4-1] of var 0..1: $DX " >> /tmp/$file.mzn;
   echo "array [0..4*NK-1] of var 0..1: $DSBK " >> /tmp/$file.mzn;
   echo "constraint not ( (forall (i in 0..r-1, j in 0..3, k in 0..4-1)(DX[i,j,k]=DX$i[i,j,k])) /\ " >> /tmp/$file.mzn;
   echo " ( forall (k in 0..4*NK-1)(DSBK[k]=DSBK$i[k]) ));" >> /tmp/$file.mzn;
   echo "$DX" >> "$outSols";
   echo "$DSBK" >> "$outSols";
   ((i++));
  fi
 done
}

function solve_noshift() {
 file=$1
 outSols=$2
 cat Heuristics/normal >> /tmp/$file.mzn
 DX="Not empty"
 i=0
 while [[ $DX != '' ]]; do
  $MINIZINC_DIR/mzn2fzn /tmp/$file.mzn -D "KEY_BITS=$KB;r=$n; objStep1=$obj";
  sh -c "time $PICAT_DIR/picat $PICAT_DIR/lib/fzn_picat_sat.pi /tmp/$file.fzn " 2&> /tmp/$file-time;
  DX=$(cat /tmp/$file-time | grep 'DX' | sed "s/DX/DX$i/g" );
  DK=$(cat /tmp/$file-time | grep 'DK' | sed "s/DK/DK$i/g" );
  DZ=$(cat /tmp/$file-time | grep 'DZ' | sed "s/DZ/DZ$i/g" );

  time=$(cat /tmp/$file-time | grep 'user\|sys' );
  echo "$time" >> "$outSols";
  if [[ $DX != '' ]]; then
   echo "array [0..r-1, 0..3, 0..3] of var 0..1: $DX" >> /tmp/$file.mzn;
   echo "array [0..r-1, 0..3, 0..4] of var 0..1: $DK" >> /tmp/$file.mzn;
   echo "array [0..r-2, 0..3, 0..3] of var 0..1: $DZ" >> /tmp/$file.mzn;
   echo "constraint not ( (forall (i in 0..r-1, j in 0..3, k in 0..3)(DX[i,j,k]=DX$i[i,j,k])) /\ " >> /tmp/$file.mzn;
   echo "               (forall (i in 0..r-1, j in 0..3, k in 0..3)(DK[i,j,k]=DK$i[i,j,k])) /\ " >> /tmp/$file.mzn;
   echo "                (forall (i in 0..r-2, j in 0..3, k in 0..3)(DZ[i,j,k]=DZ$i[i,j,k]) ) );  " >> /tmp/$file.mzn;
   echo "$DX" >> "$outSols";
   echo "$DK" >> "$outSols";
   echo "$DZ" >> "$outSols";
   ((i++));
  fi
 done;
}

echo "S1Diff, no shift"
 echo "Step 1..."
 model="S1Diff"
 out=$model-no-shift
 T=$(date +%s%N);
 file=$model$T;
 cp $model.mzn /tmp/$file.mzn;
 solve_noshift $file $out
 echo "Step 2..."
 java -Xmx4096m -cp .:$CHOCO_DIR/choco-solver-3.3.3-with-dependencies.jar CryptoMain_all $out $KB $n $obj 1 ;

echo "S1Diff, shift"
 echo "Step 1..."
 model="S1Diff"
 out=$model-shift
 T=$(date +%s%N);
 file=$model$T;
 cp $model.mzn /tmp/$file.mzn;
 solve_shift $file $out
 echo "Step 2..."
 java -Xmx4096m -cp .:$CHOCO_DIR/choco-solver-3.3.3-with-dependencies.jar CryptoMain_all $out $KB $n $obj 2 ;
echo "S1XOR, no shift"
 echo "Step 1..."
 model="S1XOR"
 out=$model-no-shift
 T=$(date +%s%N);
 file=$model$T;
 cp $model.mzn /tmp/$file.mzn;
 cat XORFiles/$(( KB/32))-$n.txt >> /tmp/$file.mzn;
 solve_noshift $file $out
 echo "Step 2..."
 java -Xmx4096m -cp .:$CHOCO_DIR/choco-solver-3.3.3-with-dependencies.jar CryptoMain_all $out $KB $n $obj 1 ;
echo "S1XOR, shift"
 echo "Step 1..."
 model="S1XOR"
 out=$model-shift
 T=$(date +%s%N);
 file=$model$T;
 cp $model.mzn /tmp/$file.mzn;
 cat XORFiles/$(( KB/32))-$n.txt >> /tmp/$file.mzn;
 solve_shift $file $out
 echo "Step 2..."
 java -Xmx4096m -cp .:$CHOCO_DIR/choco-solver-3.3.3-with-dependencies.jar CryptoMain_all $out $KB $n $obj 2 ;
